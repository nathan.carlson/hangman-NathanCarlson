#!/usr/bin/python3
# hangman by Nathan Carlson
# 12/1/2016

import random   # imports random library

wordlist = []
def startGame():
    global wordlist # call global variable into function
    fileopen = open("wordslist.txt", "rt")  # opens wordslist and reads as text
    wordlist = fileopen.readlines() # creates a list from the text file
    fileopen.close()
    wordlist = [i.replace('\n','') for i in wordlist]  # removes the \n in the list
    print(wordlist)
    rangeOfLines = len(wordlist)  # number of lines in list

    randomChoice = random.randint(1, rangeOfLines)  # selects a random number
    print("\n\n", wordlist[randomChoice].upper())  # make upper case





def instructions():
        # function to start instructions at beginning of game
        # This function will give user a set of instructions on
        # how to play this game and the limitations of the game.
    print("\tHangman by Nathan Carlson \n\n")

    print("Guess one letter at a time to uncover word\n"\
    "If you guess a wrong letter, 'Mr. Wrongletter' will start to die\n"\
    "You will have 7 incorrect chances before he dies, so help"\
    " him out and save Mr. Wrongletter")
    startGame()

instructions()  # call function
